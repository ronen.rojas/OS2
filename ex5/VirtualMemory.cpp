#include "VirtualMemory.h"
#include "PhysicalMemory.h"
#include <cmath>

using namespace std;

uint64_t getTableAddress(uint64_t virtualAddress, int layerNum);

uint64_t getOffset(uint64_t virtualAddress);

uint64_t getNbits(uint64_t number, int n);

uint64_t pageAddressing(uint64_t virtualAddress);

uint64_t getCyclicalDistance(uint64_t mainPage, uint64_t potentialSwappedPage);

struct DfsContext {
    word_t frameMax;
    int standingFrame;
    uint64_t standingPage;
    int lowestEmptyTable;
    uint64_t lowestEmptyTableAddress;
    uint64_t pageWithMaxCyclicDistance;
    int frameWithMaxCyclicDistance;
    uint64_t frameWithMaxCyclicDistanceAddress;
    uint64_t maxCyclicDistance;

};

void findFrameDFS(DfsContext *dfsContext, int layer, word_t frameIndex, uint64_t fatherAddress, uint64_t callerRow) {
    if (layer == TABLES_DEPTH) {
        uint64_t dist = getCyclicalDistance(dfsContext->standingPage, callerRow);
        if (dist > dfsContext->maxCyclicDistance) {
            dfsContext->frameWithMaxCyclicDistance = frameIndex;
            dfsContext->frameWithMaxCyclicDistanceAddress = fatherAddress;
            dfsContext->pageWithMaxCyclicDistance = callerRow;
            dfsContext->maxCyclicDistance = dist;
        }
        return;
    }
    bool allZeros = true;
    for (int row = 0; row < PAGE_SIZE; row++) {
        word_t frameNext;
        PMread(static_cast<uint64_t>(frameIndex * PAGE_SIZE + row), &frameNext);

        if (frameNext != 0) {
            allZeros = false;
            findFrameDFS(dfsContext, layer + 1, frameNext, static_cast<uint64_t>(frameIndex * PAGE_SIZE + row),
                         (callerRow << OFFSET_WIDTH) + row);

            if (frameNext > dfsContext->frameMax) {
                dfsContext->frameMax = frameNext;
            }
        }
    }
    if (allZeros) {
        if (dfsContext->standingFrame != frameIndex) {
            if (frameIndex < dfsContext->lowestEmptyTable) {
                dfsContext->lowestEmptyTable = frameIndex;
                dfsContext->lowestEmptyTableAddress = fatherAddress;
            }
        }


    }

}


void clearTable(uint64_t frameIndex) {
    for (uint64_t i = 0; i < PAGE_SIZE; ++i) {
        PMwrite(frameIndex * PAGE_SIZE + i, 0);
    }
}

void VMinitialize() {
    clearTable(0);
}


int VMread(uint64_t virtualAddress, word_t *value) {
    if (virtualAddress >= VIRTUAL_MEMORY_SIZE) {
        return 0;
    }
    uint64_t pageIndex = pageAddressing(virtualAddress);
    PMread(pageIndex * PAGE_SIZE + getOffset(virtualAddress), value);
    return 1;
}

int VMwrite(uint64_t virtualAddress, word_t value) {
    if (virtualAddress >= VIRTUAL_MEMORY_SIZE) {
        return 0;
    }
    uint64_t pageIndex = pageAddressing(virtualAddress);
    PMwrite(pageIndex * PAGE_SIZE + getOffset(virtualAddress), value);
    return 1;
}


uint64_t getNbits(uint64_t number, int n) {
    return number & ((1ULL << n) - 1);
}


uint64_t getTableAddress(uint64_t virtualAddress, int layerNum) {
    return getNbits(virtualAddress >> (OFFSET_WIDTH * (TABLES_DEPTH - layerNum)), OFFSET_WIDTH);

}

uint64_t getOffset(uint64_t virtualAddress) {
    return getNbits(virtualAddress, OFFSET_WIDTH);
}

uint64_t pageAddressing(uint64_t virtualAddress) {
    uint64_t tableRow;
    uint64_t pageIndex = 0;
    word_t pageTemp = 0;
    for (int i = 0; i < TABLES_DEPTH; ++i) {
        tableRow = getTableAddress(virtualAddress, i);
        PMread(pageIndex * PAGE_SIZE + tableRow, &pageTemp);
        if (pageTemp == 0) {
            DfsContext dfsContext = {.frameMax = 0,
                    .standingFrame = static_cast<int>(pageIndex),
                    .standingPage = virtualAddress >> OFFSET_WIDTH,
                    .lowestEmptyTable = RAM_SIZE,
                    .lowestEmptyTableAddress= 0LL,
                    .pageWithMaxCyclicDistance= 0,
                    .frameWithMaxCyclicDistance=0,
                    .frameWithMaxCyclicDistanceAddress=0LL,
                    .maxCyclicDistance = 0};

            findFrameDFS(&dfsContext, 0, 0, 0, 0);

            // Priority 2 ( But this is initialization period for the physical memory)
            if ((dfsContext.frameMax + 1) < NUM_FRAMES) {
                pageTemp = dfsContext.frameMax + 1;
                PMwrite(pageIndex * PAGE_SIZE + tableRow, pageTemp);

            }
                // Priority 1
            else if (dfsContext.lowestEmptyTable < RAM_SIZE) {
                // new frame
                pageTemp = dfsContext.lowestEmptyTable;
                // deleting its reference
                PMwrite(dfsContext.lowestEmptyTableAddress, 0);
                PMwrite(pageIndex * PAGE_SIZE + tableRow, pageTemp);
            } else {
                // Priority 3
                pageTemp = dfsContext.frameWithMaxCyclicDistance;
                //std::cout << "PMevict(" << pageTemp << ", " << dfsContext.pageWithMaxCyclicDistance << ")\n";
                PMevict(static_cast<uint64_t>(pageTemp),
                        static_cast<uint64_t>(dfsContext.pageWithMaxCyclicDistance));
                // deleting its reference
                PMwrite(dfsContext.frameWithMaxCyclicDistanceAddress, 0);

                // reference correct page
                PMwrite(pageIndex * PAGE_SIZE + tableRow, pageTemp);
                // Deleting its content only it is a table
                if (i < TABLES_DEPTH - 1) {
                    for (int j = 0; j < PAGE_SIZE; j++) {
                        PMwrite(static_cast<uint64_t>(pageTemp * PAGE_SIZE + j), 0);
                    }
                }
            }

            pageIndex = static_cast<uint64_t>(pageTemp);
            if (i == TABLES_DEPTH - 1) {
                // We have reached a leaf
                //std::cout << "PMrestore(" << pageIndex << ", " << (virtualAddress >> OFFSET_WIDTH) << ")\n";
                PMrestore(pageIndex, (virtualAddress >> OFFSET_WIDTH));
            }

        } else {
            pageIndex = static_cast<uint64_t>(pageTemp);
        }
    }
    return pageIndex;
}

uint64_t getCyclicalDistance(uint64_t mainPage, uint64_t potentialSwappedPage) {
    uint64_t distance;
    if (mainPage > potentialSwappedPage) {
        distance = mainPage - potentialSwappedPage;
    } else {
        distance = potentialSwappedPage- mainPage;
    }
    return static_cast<uint64_t>(fmin(distance, NUM_PAGES - distance));
}



