
#include <cstdio>
#include <cassert>
#include <iostream>
#include <string>
#include "VirtualMemory.h"
#include "PhysicalMemory.h"


std::string l2bs(long long n);

void check_value_frame(uint64_t frameIndex, uint64_t offset, word_t value);

void fill_value_frame(uint64_t frameIndex, uint64_t offset, word_t value);

int main(int argc, char **argv) {
    VMinitialize();
//
//    //  command : VMread(13,&value);
//    // fill frame 0
//    fill_value_frame(0, 0, 1);
//    fill_value_frame(0, 1, 0);
//    // fill frame 1
//    fill_value_frame(1, 0, 0);
//    fill_value_frame(1, 1, 2);
//    // fill  frame 2
//    fill_value_frame(2, 0, 0);
//    fill_value_frame(2, 1, 3);
//    // fill frame 3
//    fill_value_frame(3, 0, 4);
//    fill_value_frame(3, 1, 0);
//    // fill frame 4
//    fill_value_frame(4, 1, 3);
//    // ---------------------
//    word_t value;
//    VMread(13, &value);
//
//    if (value != 3) { std::cout << "Wrong value: VMread(13, &value)\n"; }
//    else { std::cout << "Success in VMread(13, &value)\n"; }


    //  command : VMread(6,&value);
    // fill frame 0
    fill_value_frame(0, 0, 1);
    fill_value_frame(0, 1, 0);

    // fill frame 1
    fill_value_frame(1, 0, 5);
    fill_value_frame(1, 1, 2);

    // fill frame 2
    fill_value_frame(2, 0, 0);
    fill_value_frame(2, 1, 3);

    // fill frame 3
    fill_value_frame(3, 0, 4);
    fill_value_frame(3, 1, 0);

    // fill frame 4
    fill_value_frame(4, 0, 0);
    fill_value_frame(4, 1, 3);


    // fill frame 5
    fill_value_frame(5, 0, 0);
    fill_value_frame(5, 1, 6);

    // fill frame 6
    fill_value_frame(6, 0, 0);
    fill_value_frame(6, 1, 7);

    // fill frame 7
    fill_value_frame(7, 0, 6);
    fill_value_frame(7, 1, 5);
    word_t value;

    //VMread(15, &value);
    VMread(31, &value);

    /*if (value != 6) { std::cout << "Wrong value VMread(6, &value)\n"; }
    else { std::cout << "Success in VMread( 6, &value)\n"; }

    /*
    //  command : VMread(6,&value);
    // fill frame 0
    fill_value_frame(0, 0, 1);
    fill_value_frame(0, 1, 4);

    // fill frame 1
    fill_value_frame(1, 0, 5);
    fill_value_frame(1, 1, 0);

    // fill frame 2
    fill_value_frame(2, 0, 0);
    fill_value_frame(2, 1, 7);

    // fill frame 3
    fill_value_frame(3, 0, 0);
    fill_value_frame(3, 1, 2);

    // fill frame 4
    fill_value_frame(4, 1, 0);
    fill_value_frame(4, 1, 3);

    // fill frame 5
    fill_value_frame(5, 0, 0);
    fill_value_frame(5, 1, 6);

    // fill frame 6
    fill_value_frame(6, 0, 0);
    fill_value_frame(6, 1, 0);

    // fill frame 7
    fill_value_frame(7, 0, 6);
    fill_value_frame(7, 1, 5);
    */
    /*VMread(31, &value);

    if (value != 5) { std::cout << "Wrong value VMread(31,&value);\n"; }
    else { std::cout << "Success in VMread(31, &value)\n"; }
    */
    return 0;
}


void check_value_frame(uint64_t frameIndex, uint64_t offset, word_t value) {
    word_t value_comp;
    PMread(frameIndex * PAGE_SIZE + offset, &value_comp);
    if (value_comp != value) { std::cout << "Wrong value Frame: " << frameIndex << "offset : " << offset << std::endl; }
}

void fill_value_frame(uint64_t frameIndex, uint64_t offset, word_t value) {
    PMwrite(frameIndex * PAGE_SIZE + offset, value);
}

std::string l2bs(long long n) {
    char result[(sizeof(unsigned long) * 8) + 1];
    unsigned index = sizeof(unsigned long) * 8;
    result[index] = '\0';

    do result[--index] = '0' + (n & 1);
    while (n >>= 1);

    return std::string(result + index);
}

long long int bs2ll(std::string s) {
    return std::stoll(s, nullptr, 2);
}
