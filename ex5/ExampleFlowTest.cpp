
#include <cstdio>
#include <cassert>
#include <iostream>
#include <string>
#include "VirtualMemory.h"
#include "PhysicalMemory.h"


std::string l2bs(long long n);

void check_value_frame(uint64_t frameIndex, uint64_t offset, word_t value);

void fill_value_frame(uint64_t frameIndex, uint64_t offset, word_t value);

int main(int argc, char **argv) {
    VMinitialize();

    // ---------------------
    // Flow Example pages : 1 - 13
    std::cout << "Check VMwrite(13, 3)\n";
    VMwrite(13, 3);

    // Check frame 0
    check_value_frame(0, 0, 1);
    check_value_frame(0, 1, 0);

    // Check frame 1
    check_value_frame(1, 0, 0);
    check_value_frame(1, 1, 2);

    // Check frame 2
    check_value_frame(2, 0, 0);
    check_value_frame(2, 1, 3);

    // Check frame 3
    check_value_frame(3, 0, 4);
    check_value_frame(3, 1, 0);

    // Check frame 4
    check_value_frame(4, 0, 0);
    check_value_frame(4, 1, 3);
    // ---------------------


    // ---------------------
    // Flow Example p14
    std::cout << "\nCheck VMread(13, &value);\n";
    word_t value;
    VMread(13, &value);

    if (value != 3) { std::cout << "Wrong value VMread(13, &value)\n"; }
    // Check frame 0
    check_value_frame(0, 0, 1);
    check_value_frame(0, 1, 0);

    // Check frame 1
    check_value_frame(1, 0, 0);
    check_value_frame(1, 1, 2);

    // Check frame 2
    check_value_frame(2, 0, 0);
    check_value_frame(2, 1, 3);

    // Check frame 3
    check_value_frame(3, 0, 4);
    check_value_frame(3, 1, 0);

    // Check frame 4
    check_value_frame(4, 1, 3);
    // ---------------------


    // ---------------------
    // Flow Example pages : 15 - 16
    std::cout << "\nCheck VMread(6, &value);\n";
    VMread(6, &value);

    check_value_frame(0, 0, 1);
    check_value_frame(0, 1, 0);

    // Check frame 1
    check_value_frame(1, 0, 5);
    check_value_frame(1, 1, 2);

    // Check frame 2
    check_value_frame(2, 0, 0);
    check_value_frame(2, 1, 3);

    // Check frame 3
    check_value_frame(3, 0, 4);
    check_value_frame(3, 1, 0);

    // Check frame 4
    check_value_frame(4, 1, 3);

    // Check frame 5
    check_value_frame(5, 0, 0);
    check_value_frame(5, 1, 6);

    // Check frame 6
    check_value_frame(6, 0, 0);
    check_value_frame(6, 1, 7);
    // ---------------------


    // ---------------------
    // Flow Example page 17 - 27
    std::cout << "\nCheck VMread(31, &value);\n";
    VMread(31, &value);

    check_value_frame(0, 0, 1);
    check_value_frame(0, 1, 4);

    // Check frame 1
    check_value_frame(1, 0, 5);
    check_value_frame(1, 1, 0);

    // Check frame 2
    check_value_frame(2, 0, 0);
    check_value_frame(2, 1, 7);

    // Check frame 3
    check_value_frame(3, 0, 0);
    check_value_frame(3, 1, 2);

    // Check frame 4
    check_value_frame(4, 0, 0);
    check_value_frame(4, 1, 3);

    // Check frame 5
    check_value_frame(5, 0, 0);
    check_value_frame(5, 1, 6);

    // Check frame 6
    check_value_frame(6, 0, 0);
    check_value_frame(6, 1, 0);
    // ---------------------
    return 0;
}

///



void check_value_frame(uint64_t frameIndex, uint64_t offset, word_t value) {
    word_t value_comp;
    PMread(frameIndex * PAGE_SIZE + offset, &value_comp);
    if (value_comp != value) {
        std::cout << "Wrong value frame: " << frameIndex << " offset : " << offset << std::endl;
    }
}

void fill_value_frame(uint64_t frameIndex, uint64_t offset, word_t value) {
    PMwrite(frameIndex * PAGE_SIZE + offset, value);
}

std::string l2bs(long long n) {
    char result[(sizeof(unsigned long) * 8) + 1];
    unsigned index = sizeof(unsigned long) * 8;
    result[index] = '\0';

    do result[--index] = '0' + (n & 1);
    while (n >>= 1);

    return std::string(result + index);
}

long long int bs2ll(std::string s) {
    return std::stoll(s, nullptr, 2);
}
