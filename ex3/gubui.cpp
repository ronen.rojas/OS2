#include "MapReduceFramework.h"
#include "Barrier.h"
#include <pthread.h>
#include <cstdio>
#include <atomic>
#include <cstdlib>
#include <algorithm>
#include <semaphore.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>

typedef std::vector<IntermediateVec> VecOfVecs;


struct Context {
    InputVec input_vec;
    IntermediateVec intermediate_vec;
    VecOfVecs vec_of_vecs;
    OutputVec *output_vec;
    const MapReduceClient *client;
    std::atomic<int> *atomic_counter;
    Barrier *barrier;
    int num_of_threads;
    sem_t *vec_count_mutex;
    pthread_mutex_t *read_and_write_mutex;
    bool finish_shuffling;
    int input_vec_size;
};

struct ThreadContext {
    int threadID;
    Context *context;
};


void map_stage(const ThreadContext *tc);

IntermediatePair &find_biggest_key(const ThreadContext *tc, IntermediatePair &max_intermediate);

IntermediateVec prepare_inner_vec_with_same_key(const ThreadContext *tc, const IntermediatePair &max_intermediate);

void pushing_the_inner_vec(const ThreadContext *tc, const IntermediateVec &inner_vector);

void reduce_action(const ThreadContext *tc);

void emit2(K2 *key, V2 *value, void *context) {
    Context *tc = (Context *) context;
//    IntermediatePair const intermediate_pair = std::make_pair(key, value);
    tc->intermediate_vec.push_back((const IntermediatePair) std::make_pair(key, value));
}


void emit3(K3 *key, V3 *value, void *context) {
    Context *tc = (Context *) context;
//    OutputPair const output_pair = std::make_pair(key, value);
    tc->output_vec[0].push_back((const OutputPair) std::make_pair(key, value));
};

bool com(const IntermediatePair first_pair, const IntermediatePair second_pair) {
    return first_pair.first->operator<(*second_pair.first);
}

void *thread_func(void *arg) {

    ThreadContext *tc = (ThreadContext *) arg;

    map_stage(tc);

    //makes sure no thread continues before all threads arrived at the barrier
    tc->context[tc->threadID].barrier->barrier();

    //After the barrier, the thread with id=0 will move on to the Shuffle phase, while the rest will skip it and move
    // directly to the Reduce phase.
    if (tc->threadID == 0) {
        IntermediatePair max_intermediate_pair;

        bool all_intermediate_vecs_are_empty = false;

        while (!all_intermediate_vecs_are_empty) {
            all_intermediate_vecs_are_empty = true;
            IntermediateVec inner_vector;
            //finding some key to initialized the biggest key before starting compare sizes

            for (int i = 0; i < tc[tc->threadID].context->num_of_threads; ++i) {
                if (!tc->context[i].intermediate_vec.empty()) {
                    max_intermediate_pair = tc->context[i].intermediate_vec.back();
                    all_intermediate_vecs_are_empty = false;
                    break;
                }
            }
            //if all the vecs are empty, so the thread 0 finished its shuffle and he escape from the loop and join
            // the other threads to do the reduces
            if (all_intermediate_vecs_are_empty) {
                continue;
            }
            max_intermediate_pair = find_biggest_key(tc, max_intermediate_pair);

            inner_vector = prepare_inner_vec_with_same_key(tc, max_intermediate_pair);

            if (pthread_mutex_lock(tc->context[0].read_and_write_mutex) != 0) {
                fprintf(stderr, "[[read_and_write_mutex]] error on pthread_mutex_lock");
                exit(-1);
            }

            //touching the common resource of all the threads - the vec_of_vecs
            pushing_the_inner_vec(tc, inner_vector);


            if (pthread_mutex_unlock(tc->context[0].read_and_write_mutex) != 0) {
                fprintf(stderr, "[[read_and_write_mutex]] error on pthread_mutex_unlock");
                exit(-1);
            }
            if (sem_post(tc->context[0].vec_count_mutex) != 0) {
                printf("semafore post vec_count_mutex error");
                exit(-1);
            }

        }
        // a flag for the other threads that the Shuffle phase is finished
        tc->context[tc->threadID].finish_shuffling = true;
    }

    //the reduce stage:
    while (!tc->context[0].finish_shuffling || !tc->context[0].vec_of_vecs.empty()) {

        if (sem_wait(tc->context[tc->threadID].vec_count_mutex) != 0) {
            printf("semafore wait vec_count_mutex error");
            exit(-1);
        }

        if (pthread_mutex_lock(tc->context[tc->threadID].read_and_write_mutex) != 0) {
            fprintf(stderr, "[[read_and_write_mutex]] error on pthread_mutex_lock");
            exit(-1);
        }

        //now it reads from the vec_of_vecs
        reduce_action(tc);

        if (pthread_mutex_unlock(tc->context[tc->threadID].read_and_write_mutex) != 0) {
            fprintf(stderr, "[[read_and_write_mutex]] error on pthread_mutex_unlock");
            exit(-1);
        }
    }

    //to release the other threads that still stuck in the reduce stage, despite the work is done
    if (sem_post(tc->context[tc->threadID].vec_count_mutex) != 0) {
        printf("semafore post vec_count_mutex error");
        exit(-1);
    }
    return 0;
}

void reduce_action(const ThreadContext *tc) {
    if (!tc->context[0].vec_of_vecs.empty()) {

        IntermediateVec vec_to_reduce = tc->context[0].vec_of_vecs.back();
        tc->context[tc->threadID].client->reduce((const IntermediateVec *) &vec_to_reduce, &tc->context[0]);
        tc->context[0].vec_of_vecs.pop_back();
    }
}

void pushing_the_inner_vec(const ThreadContext *tc,
                           const IntermediateVec &inner_vector) { tc->context[0].vec_of_vecs.push_back(inner_vector); }

IntermediateVec prepare_inner_vec_with_same_key(const ThreadContext *tc, const IntermediatePair &max_intermediate) {
    IntermediateVec inner_vector;
    for (int i = 0; i < tc[tc->threadID].context->num_of_threads; ++i) {
        while (!tc->context[i].intermediate_vec.empty() &&
               (!com((const IntermediatePair) max_intermediate,
                     (const IntermediatePair) tc->context[i].intermediate_vec.back()) &&
                !com((const IntermediatePair) tc->context[i].intermediate_vec.back(),
                     (const IntermediatePair) max_intermediate))) {
            inner_vector.push_back(tc->context[i].intermediate_vec.back());
            tc->context[i].intermediate_vec.pop_back();
        }
    }
    return inner_vector;
}

IntermediatePair &find_biggest_key(const ThreadContext *tc,
                                   IntermediatePair &max_intermediate) {//find the biggest key among all intermediate vectors
    for (int i = 0; i < tc[tc->threadID].context->num_of_threads; ++i) {
        if (!tc->context[i].intermediate_vec.empty()) {
            if (com((const IntermediatePair) max_intermediate,
                    (const IntermediatePair) tc->context[i].intermediate_vec.back())) {
                max_intermediate = tc->context[i].intermediate_vec.back();
            }
        }
    }
    return max_intermediate;
}

void map_stage(const ThreadContext *tc) {
    K1 *key;
    V1 *value;
    int old_value;
    while (true) {
        old_value = (*(tc->context[tc->threadID].atomic_counter))++;
        if (old_value >= tc->context[tc->threadID].input_vec_size) {
            break;
        } else {
            key = tc->context[tc->threadID].input_vec[old_value].first;
            value = tc->context[tc->threadID].input_vec[old_value].second;

            (*tc->context[tc->threadID].client).map((const K1 *) key, (const V1 *) value, &tc->context[tc->threadID]);
        }
    }
    if (!tc->context[tc->threadID].intermediate_vec.empty()) {
        sort(tc->context[tc->threadID].intermediate_vec.begin(), tc->context[tc->threadID].intermediate_vec.end(), com);
    }
}


void runMapReduceFramework(const MapReduceClient &client, const InputVec &inputVec, OutputVec &outputVec,
                           int multiThreadLevel) {
    VecOfVecs vec_of_vecs;
    IntermediateVec intermediate_vec;
    std::atomic<int> atomic_counter(0);
    sem_t vec_count_mutex;
    pthread_mutex_t read_and_write_mutex;
    ThreadContext threadContext[multiThreadLevel];
    pthread_t threads[multiThreadLevel - 1];
    Context contexts[multiThreadLevel];
    Barrier barrier(multiThreadLevel);
    int input_vec_size = (int) (inputVec.size());

    if (pthread_mutex_init(&read_and_write_mutex, NULL) != 0) {
        fprintf(stderr, "[[read_and_write_mutex]] error on pthread_mutex_init");
        exit(-1);
    }
    read_and_write_mutex = PTHREAD_MUTEX_INITIALIZER;


    if (sem_init(&vec_count_mutex, 0, 0) != 0) {
        fprintf(stderr, "[[vec_count_mutex]] error on sem_init");
        exit(-1);
    }

    for (int i = 0; i < multiThreadLevel; ++i) {
        contexts[i] = {inputVec, intermediate_vec, vec_of_vecs, &outputVec, &client, &atomic_counter, &barrier,
                       multiThreadLevel, &vec_count_mutex, &read_and_write_mutex, false, input_vec_size};
    }

    for (int i = 0; i < multiThreadLevel - 1; ++i) {
        threadContext[i] = {i, contexts};
        if (pthread_create(threads + i, NULL, thread_func, threadContext + i) != 0) {
            fprintf(stderr, "[[thread]] error on pthread_create");
            exit(-1);
        }
    }
    //calling the main thread to help the other threads
    threadContext[multiThreadLevel - 1] = {multiThreadLevel - 1, contexts};
    thread_func(&threadContext[multiThreadLevel - 1]);

//    make sure that the main thread won't finish before all the threads finish their work
    for (int i = 0; i < multiThreadLevel - 1; ++i) {
        if (pthread_join(threads[i], NULL) != 0) {
            fprintf(stderr, "[[thread]] error on pthread_join");
            exit(-1);
        }
    }
    if (pthread_mutex_destroy(&read_and_write_mutex) != 0) {
        fprintf(stderr, "[[read_and_write_mutex]] error on pthread_mutex_destroy");
        exit(-1);
    }
    if (sem_destroy(&vec_count_mutex) != 0) {
        fprintf(stderr, "[[vec_count_mutex]] error on sem_destroy");
        exit(-1);
    }
}
