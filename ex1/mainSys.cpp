#include <iostream>
#include <iomanip>
#include "osm.h"
using namespace std;

int main() {
	unsigned int iterations = 10;
	int powerIter = 1;
	double time ;
	while (powerIter<8){
		time = osm_syscall_time(iterations);
		cout << "value for system with " << left << setw(10)<< iterations << " iterations is: " << left << setw(8) << time << " [nanoseconds]\n";
		iterations *= 10;
		powerIter += 1;
	}
    return 0;
}
