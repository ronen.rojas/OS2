#include <iostream>
#include <iomanip>
#include "osm.h"
using namespace std;

int main() {
	unsigned int iterations = 1000;
	int powerIter = 3;
	double time ;
	while (powerIter<10){
		time = osm_function_time(iterations);
		cout << "value for function with " << left << setw(10)<< iterations << " iterations is: " << left << setw(8) << time << " [nanoseconds]\n";
		iterations *= 10;
		powerIter += 1;
	}
    return 0;
}
