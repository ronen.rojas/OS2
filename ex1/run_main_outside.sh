make clean
make
g++ -std=c++11 -Wall main.cpp     -o mainExec osm.o
g++ -std=c++11 -Wall mainOp.cpp   -o opExec   osm.o
g++ -std=c++11 -Wall mainFunc.cpp -o funcExec osm.o
g++ -std=c++11 -Wall mainSys.cpp  -o sysExec  osm.o

echo Running all three
./mainExec
echo 
echo Running Operation call
./opExec > operation_outside.log
echo 
echo Running Function call
./funcExec > function_outside.log
echo 
echo Running System call
./sysExec > system_outside.log
echo 
echo Creating graph
python3 create_graph.py
echo
echo making tar
make tar

