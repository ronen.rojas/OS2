#include <iostream>
#include <iomanip>
#include "osm.h"
using namespace std;

int main() {
    double time =osm_operation_time(1000000);
    cout << "value for operation with 1000000 iters is: " <<  left << setw(8) << time << " [nanoseconds]\n" ;
    time = osm_function_time(1000000);
    cout << "value for function  with 1000000 iters is: " <<  left << setw(8) << time << " [nanoseconds]\n" ;
    time = osm_syscall_time(1000000);
    cout << "value for syscall   with 1000000 iters is: " <<  left << setw(8) << time << " [nanoseconds]\n" ;
    return 0;
}
