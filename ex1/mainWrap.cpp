#include <iostream>
#include <iomanip>
#include "osm.h"
#include <climits>
using namespace std;

int main() {
	unsigned int iterations = UINT_MAX - 1  ;
	double time ;
	
	
	time = osm_operation_time(iterations);
	cout << "value for operation with " << left << setw(10)<< iterations << " iterations is: " <<      left << setw(8) << time << " [nanoseconds]\n";
	
    return 0;
}
