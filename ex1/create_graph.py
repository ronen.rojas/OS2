import numpy as np
import matplotlib.pyplot as plt

OP_IDX = 0
FUNC_IDX = 1
SYS_IDX = 2


def get_measurements(file_name):
    lines = [line for line in open(file_name).read().split('\n') if line]
    last_line = lines[-1].split()
    iter_num = None
    time_value = None
    for word in last_line:
        if word.isnumeric():
            if iter_num is None:
                iter_num = int(word)
        else:
            if word.replace('.', '', 1).isdigit():
                time_value = float(word)
    return iter_num, time_value


# update measurements
n_groups = 3
iter_vm, time_vm = tuple(zip(get_measurements('operation_inside.log'),
                             get_measurements('function_inside.log'),
                             get_measurements('system_inside.log')))
iter_linux, time_linux = tuple(zip(get_measurements('operation_outside.log'),
                                   get_measurements('function_outside.log'),
                                   get_measurements('system_outside.log')))

# create plot
fig, ax = plt.subplots()
ax.set_yscale('log')
index = np.arange(n_groups)
bar_width = 0.35
opacity = 0.8
ax.grid(color='k', linestyle='-', linewidth=0.5, axis='y', which='minor',
        alpha=0.2)
ax.grid(color='k', linestyle='-', linewidth=0.5, axis='y', which='major',
        alpha=0.5)

# create bars
rect1 = plt.bar(index, time_vm, bar_width, alpha=opacity, color='b',
                label='inside VM')
rect2 = plt.bar(index + bar_width, time_linux, bar_width, alpha=opacity,
                color='g', label='outside VM')

# annotate bars
for idx, time_v in enumerate(time_vm):
    if idx != SYS_IDX:
        plt.annotate('{:0.1f}'.format(time_v), xy=(idx - bar_width*0.25,
                                                   time_v))
    else:
        plt.annotate(str(int(time_v)), xy=(idx - bar_width*0.25, time_v))
for idx, time_v in enumerate(time_linux):
    if idx != SYS_IDX:
        plt.annotate('{:0.1f}'.format(time_v), xy=(idx + bar_width*0.75, time_v))
    else:
        plt.annotate(str(int(time_v)), xy=(idx + bar_width*0.75, time_v))


# labels
plt.ylabel('time [ns]')
plt.title('Running time of basic operation/calls in nanoseconds')
plt.xticks(index + 0.5*bar_width,
           ('Arithmetic operation \n({:0.0} iterations)'.format(float(
               iter_vm[OP_IDX])),
            'Function call\n({:0.0} iterations)'.format(
                float(iter_vm[FUNC_IDX])),
            'System call\n({:0.0} iterations)'.format(
                float(iter_vm[SYS_IDX]))))
plt.legend()
plt.tight_layout()
fig.savefig('graph.png')
