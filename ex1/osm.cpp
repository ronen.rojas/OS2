#include <iostream>
#include <sys/time.h>
#include "osm.h"
#define UNROLL_FACTOR 20

double getTimeInNanoSec(unsigned int iterations, const timeval &start, const timeval &end);

int osm_init() {return 0;}

int osm_finalizer() {return 0;}

void some_function() {}

double osm_operation_time(unsigned int iterations) {

    struct timeval start, end;
    unsigned int i;
    unsigned int unroll_iterations;
    int sum __attribute__((unused)) = 0;

    if (iterations == 0) {iterations = 1000;}

    unroll_iterations = iterations / UNROLL_FACTOR;
    if (iterations % UNROLL_FACTOR != 0) { unroll_iterations +=1;}

    if (gettimeofday(&start, NULL) == -1) {return -1;}

    for (i = 0; i < unroll_iterations; i += 1) {
        sum++; // #1
		sum++; // #2
		sum++; // #3
		sum++; // #4
		sum++; // #5
		sum++; // #6
		sum++; // #7
		sum++; // #8
		sum++; // #9
		sum++; // #10
		sum++; // #11
		sum++; // #12
		sum++; // #13
		sum++; // #14
		sum++; // #15
		sum++; // #16
		sum++; // #17
		sum++; // #18
		sum++; // #19
		sum++; // #20
    }

    if (gettimeofday(&end, NULL) == -1) {return -1;}

    return getTimeInNanoSec(iterations, start, end);
}

double osm_syscall_time(unsigned int iterations) {

    struct timeval start, end;
    unsigned int i;
    unsigned int unroll_iterations;

    if (iterations == 0) {iterations = 1000;}

    unroll_iterations = iterations / UNROLL_FACTOR;
    if (iterations % UNROLL_FACTOR != 0) { unroll_iterations +=1;}

    if (gettimeofday(&start, NULL) == -1) { return -1;}

    for (i = 0; i < unroll_iterations; i += 1) {
        OSM_NULLSYSCALL; // #1
		OSM_NULLSYSCALL; // #2
		OSM_NULLSYSCALL; // #3
		OSM_NULLSYSCALL; // #4
		OSM_NULLSYSCALL; // #5
		OSM_NULLSYSCALL; // #6
		OSM_NULLSYSCALL; // #7
		OSM_NULLSYSCALL; // #8
		OSM_NULLSYSCALL; // #9
		OSM_NULLSYSCALL; // #10
		OSM_NULLSYSCALL; // #11
		OSM_NULLSYSCALL; // #12
		OSM_NULLSYSCALL; // #13
		OSM_NULLSYSCALL; // #14
		OSM_NULLSYSCALL; // #15
		OSM_NULLSYSCALL; // #16
		OSM_NULLSYSCALL; // #17
		OSM_NULLSYSCALL; // #18
		OSM_NULLSYSCALL; // #19
		OSM_NULLSYSCALL; // #20
		
    }

    if (gettimeofday(&end, NULL) == -1) { return -1;}

    return getTimeInNanoSec(iterations, start, end);
}

double osm_function_time(unsigned int iterations) {
    struct timeval start, end;
    unsigned int i;
    unsigned int unroll_iterations;

    if (iterations == 0) {iterations = 1000;}

    unroll_iterations = iterations / UNROLL_FACTOR;
    if (iterations % UNROLL_FACTOR != 0) { unroll_iterations +=1;}

    if (gettimeofday(&start, NULL) == -1) { return -1;}

    for (i = 0; i < unroll_iterations; i += 1) {
        some_function(); // #1
		some_function(); // #2
		some_function(); // #3
		some_function(); // #4
		some_function(); // #5
		some_function(); // #6
		some_function(); // #7
		some_function(); // #8
		some_function(); // #9
		some_function(); // #10
		some_function(); // #11
		some_function(); // #12
		some_function(); // #13
		some_function(); // #14
		some_function(); // #15
		some_function(); // #16
		some_function(); // #17
		some_function(); // #18
		some_function(); // #19
		some_function(); // #20
    }

    if (gettimeofday(&end, NULL) == -1) { return -1; }

    return  getTimeInNanoSec(iterations, start, end);
}

double getTimeInNanoSec(unsigned int iterations, const timeval &start, const timeval &end) {
    double start_ms, end_ms, time;
    start_ms = start.tv_sec * 1000000 + start.tv_usec;
    end_ms = end.tv_sec * 1000000 + end.tv_usec;
    time = end_ms - start_ms;
    time /= iterations;
    return time * 1000;
}
