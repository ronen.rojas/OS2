+    /*
+ * (06:03:07 PM) Matan Levy:     int fdS;
+    struct sockaddr_in sa;
+//    struct hostent * host;
+//    gethostname(host_name, WA_MAX_NAME);
+//    if ((host = gethostbyname(host_name)) == nullptr)
+//    {
+//        print_error("gethostbyname", errno);
+//        exit(1);
+//    }
+    memset(&sa, 0, sizeof(struct sockaddr_in));
+
+    sa.sin_family = AF_INET;
+
+// ********************
+    sa.sin_addr.s_addr = INADDR_ANY;
+// ********************
+
+    sa.sin_port = htons(portnum);
+    // create Socket
+    if ((fdS = socket(AF_INET, SOCK_STREAM, 0)) < 0)
+    {
+        print_error("socket", errno);
+        exit(1);
+    }
+    if (bind(fdS, (struct sockaddr * ) &sa, sizeof(struct sockaddr)) < 0)
+    {
+        close(fdS);
+        print_error("bind", errno);
+        exit(1);
+    }
+    if(listen(fdS, MAX_CLIENTS ) < 0)
+    {
+        close(fdS);
+        print_error("listen", errno);
+        exit(1);
+    }
+    return (fdS);
+ * */
+

