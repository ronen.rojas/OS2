#include <iostream>
#include <zconf.h>
#include <netdb.h>
#include <cstring>
#include "whatsappio.h"
#include <sstream>


using namespace std;

int call_socket(unsigned short portnum, const char *ip);

int main(int argc, char **argv) {
    int clientSockfd;
    unsigned short portnum;
    string ip;
    string clientName;
    string portnumStr;

    // Checking number of inputs
    if (argc != 4) {
        print_client_usage();
        exit(1);
    }
    istringstream a1(argv[1]);
    istringstream a2(argv[2]);
    istringstream a3(argv[3]);
    istringstream a4(argv[3]);
    // Checking inputs
    if (!(a1 >> clientName)) {
        print_client_usage();
        exit(1);
    }
    if (!(a2 >> ip)) {
        print_client_usage();
        exit(1);
    }
    if (!(a3 >> portnum)) {
        print_client_usage();
        exit(1);
    }
    if (!(a4 >> portnumStr)) {
        print_client_usage();
        exit(1);
    }
    // Checking port number numeric
    if (!is_number(portnumStr)) {
        print_invalid_input();
        exit(1);
    }
    // Checking client name is alpha-numeric
    if (!check_name(clientName)) {
        print_invalid_input();
        exit(1);
    }

    if (clientName.size() > WA_MAX_NAME) {
        print_invalid_input();
        exit(1);
    }
    // socket initialization
    clientSockfd = call_socket(portnum, ip.c_str());
    if (clientSockfd < 0) {
        print_fail_connection();
    };

    // Client sending name
    MessageIF msgIF;
    memset(&msgIF, 0, sizeof(MessageIF));
    char nameSend[WA_MAX_NAME + 1];
    strcpy(nameSend, clientName.c_str());
    strcpy(msgIF.name, nameSend);
    msgIF.typeMsg = e_INITIAL;
    if (send(clientSockfd, &msgIF, sizeof(MessageIF), 0) < 0) {
        print_error("send", errno);
        exit(1);
    };

    // Client receiving acknowledge  - no Time-out here
    memset(&msgIF, 0, sizeof(MessageIF));
    if (recv(clientSockfd, &msgIF, sizeof(MessageIF), 0) < 0) {
        print_error("recv", errno);
        exit(1);
    };

    if (msgIF.status == e_SUCCESS && msgIF.typeMsg == e_INITIAL) {
        print_connection();
    } else {
        print_dup_connection();
        exit(1);
    }

    // Initializing file descriptors to select from
    fd_set clientinfds;
    fd_set readfds;
    FD_ZERO(&clientinfds);
    FD_SET(clientSockfd, &clientinfds);
    FD_SET(STDIN_FILENO, &clientinfds);

    // The main loop
    int ready;
    while (true) {
        FD_ZERO(&readfds);
        readfds = clientinfds;
        if ((ready = select(4, &readfds, NULL, NULL, NULL)) < 0) {
            print_error("select", errno);
            exit(1);
        }
        if (ready == 0) {
            continue;
        }
        // Receiving User input
        if FD_ISSET(STDIN_FILENO, &readfds) {
            char command[WA_MAX_INPUT];
            memset(command, 0, WA_MAX_INPUT);
            if (read(STDIN_FILENO, command, WA_MAX_INPUT) < 0) {
                print_error("read", errno);
                exit(1);
            }
            // Check syntax of message
            command_type commandT;
            string name;
            string message;
            vector<string> clients;
            string castCommand(command);
            parse_command(castCommand, commandT, name, message, clients);
            if (name.size() > WA_MAX_NAME) {
                print_invalid_input();
                continue;
            }
            if (message.size() > WA_MAX_MESSAGE) {
                print_invalid_input();
                continue;
            }
            // Decide what to do with each command
            switch (commandT) {
                case WHO: {
                    memset(&msgIF, 0, sizeof(MessageIF));
                    if (castCommand.size() == 4) {
                        msgIF.typeMsg = e_WHO;
                        if (send(clientSockfd, &msgIF, sizeof(MessageIF), 0) < 0) {
                            print_error("send", errno);
                            exit(1);
                        };
                    } else {
                        print_invalid_input();
                    }
                    break;
                }
                case CREATE_GROUP: {
                    memset(&msgIF, 0, sizeof(MessageIF));
                    msgIF.typeMsg = e_CREATE_GROUP;
                    strcpy(nameSend, name.c_str());
                    strcpy(msgIF.name, nameSend);
                    char msgSend[WA_MAX_MESSAGE + 1];
                    strcpy(msgSend, command);
                    strcpy(msgIF.msg, msgSend);
                    if (clients.empty()) {
                        print_create_group(false, false, name, name);
                        break;
                    } else if (clients.size() == 1 && clients.back() == clientName) {
                        print_create_group(false, false, name, name);
                        break;
                    }
                    if (!check_name(name)) {
                        print_create_group(false, false, name, name);
                        break;
                    }
                    bool clientValidFlag = true;
                    for (auto it = clients.begin(); it != clients.end(); ++it) {
                        if (!check_name(*it)) {
                            clientValidFlag = false;
                            break;
                        }
                        // Check if the size of a client name is not too big
                        if (it->size() > WA_MAX_NAME) {
                            clientValidFlag = false;
                            break;
                        }
                    }
                    if (!clientValidFlag) {
                        print_create_group(false, false, name, name);
                        break;
                    }
                    if (send(clientSockfd, &msgIF, sizeof(MessageIF), 0) < 0) {
                        print_error("send", errno);
                        exit(1);
                    };
                    break;
                }
                case SEND: {
                    memset(&msgIF, 0, sizeof(MessageIF));
                    msgIF.typeMsg = e_SEND;
                    strcpy(nameSend, name.c_str());
                    strcpy(msgIF.name, nameSend);

                    char msgSend[WA_MAX_MESSAGE + 1];
//                    message.pop_back();
                    strcpy(msgSend, message.c_str());
                    strcpy(msgIF.msg, msgSend);
                    if (name == clientName) {
                        print_send(false, false, name, name, message);
                        break;
                    }
                    if (!check_name(name)) {
                        print_send(false, false, name, name, message);
                        break;
                    }
                    if (message.empty()) {
                        print_send(false, false, name, name, message);
                        break;
                    }
                    if (send(clientSockfd, &msgIF, sizeof(MessageIF), 0) < 0) {
                        print_error("send", errno);
                        exit(1);
                    };
                    break;
                }
                case EXIT: {
                    memset(&msgIF, 0, sizeof(MessageIF));
                    msgIF.typeMsg = e_EXIT;
                    if (send(clientSockfd, &msgIF, sizeof(MessageIF), 0) < 0) {
                        print_error("send", errno);
                        exit(1);
                    }
                    if (close(clientSockfd) < 0) {
                        print_error("close", errno);
                        exit(1);
                    }
                    print_exit(false, name);
                    exit(1);
                }
                case INVALID: {
                    print_invalid_input();
                    break;
                }
                default : {
                    printf("default");
                }
            }
            // Receiving Server input
        } else if FD_ISSET(clientSockfd, &readfds) {
            memset(&msgIF, 0, sizeof(MessageIF));
            if (recv(clientSockfd, &msgIF, sizeof(MessageIF), 0) < 0) {
                print_error("recv", errno);
                exit(1);
            };
            switch (msgIF.typeMsg) {
                case e_CREATE_GROUP: {
                    string nameG = string(msgIF.name);
                    if (msgIF.status == e_SUCCESS && nameG.empty()) {
                        cout << "Something went wrong the server exiting !\n";
                        exit(1);
                    }
                    print_create_group(false, msgIF.status == e_SUCCESS, msgIF.msg, msgIF.name);
                    break;
                }

                case e_SEND: {
                    cout << msgIF.name << ": " << msgIF.msg << endl;
                    break;
                }
                case e_WHO: {
                    cout << msgIF.msg << endl;
                    break;
                }
                case e_STATUS: {
                    print_send(false, msgIF.status == e_SUCCESS, msgIF.name, msgIF.name, msgIF.msg);
                    break;
                }
                case e_EXIT: {
                    if (close(clientSockfd) < 0) {
                        print_error("close", errno);
                        exit(1);
                    }
                    exit(1);
                }
                default : {
                    break;
                }
            }

        }
    }
}

int call_socket(unsigned short portnum, const char *ip) {

    struct sockaddr_in sa;
    struct hostent *hp;
    int serverSockfd;
    if ((hp = gethostbyname(ip)) == NULL) {
        print_error("gethostbyname", h_errno);
        exit(1);
    }

    memset(&sa, 0, sizeof(sa));
    memcpy((char *) &sa.sin_addr, hp->h_addr, hp->h_length);
    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons((u_short) portnum);
    if ((serverSockfd = socket(hp->h_addrtype, SOCK_STREAM, 0)) < 0) {
        print_error("socket", errno);
        exit(1);
    }

    if (connect(serverSockfd, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
        close(serverSockfd);
        print_error("connect", errno);
        exit(1);
    }
    return serverSockfd;
}
