import subprocess
import time

port = '8882'
SERVER_START = 'whatsappServer ' + port
CLIENT1_CMD = 'whatsappClient client1 127.0.0.1 ' + port
CLIENT2_CMD = 'whatsappClient client2 127.0.0.1 ' + port

p_server = subprocess.Popen(SERVER_START, shell=True)  # Server is running at this point
p_client1 = subprocess.Popen(CLIENT1_CMD, shell=True, stdin=subprocess.PIPE)  # Client is running at this point
time.sleep(1)
p_client2 = subprocess.Popen(CLIENT2_CMD, shell=True)  # Client is running at this point

p_client1.stdin.write('who\n')
time.sleep(1)
print p_server.stdout.readline()

#input_commands = [CMD1, CMD2, CMD3]
#expected_outputs = [OUTPUT1, OUTPUT2, OUTPUT3]



"""
for in_cmd, ex_output in zip(input_commands, expected_outputs):
    print 'Sending command:', in_cmd
    p_client.stdin.write('Hello\n\r')
    print 'Waiting for output...'
    time.sleep(1)
    output = p_server.stdout.readline()
    print 'Received output - {} [{}]'.format(output, 'PASSED' if output == ex_output else 'FAILED')
"""
p_server.terminate()
p_client1.terminate()
p_client2.terminate()