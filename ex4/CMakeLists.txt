cmake_minimum_required(VERSION 3.8)
project(ex4)

set(CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
add_executable(server whatsappio.h whatsappio.cpp whatsappServer.cpp)
add_executable(client whatsappio.h whatsappio.cpp whatsappClient.cpp)
