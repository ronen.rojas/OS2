#include <iostream>
#include <zconf.h>
#include <netdb.h>
#include <cstring>
#include "whatsappio.h"
#include <sstream>
#include <map>
#include <set>
#include <algorithm>



unsigned short getPort(char *const *argv);

int getSocket(unsigned short portnum);

int initializeSocket(unsigned short portnum);

fd_set &initializeFdSet(fd_set &clientsfds, int serverSockfd);

fd_set &initialiseReadFds(const fd_set &clientsfds, fd_set &readfds);

bool sdtinInputCase(int socketNum, vector<int> &socketsVec, int serverSockfd);

bool generalSocketInputCase(map<int, string> &socketClientMap, vector<string> &usersVec, int serverSockfd,
                            fd_set &clientsfds, int socketNum, map<string, int> &clientSocketMap,
                            vector<int> &socketsVec, map<string, set<string>> groupClientsMap);

void userExitCase(map<string, int> &clientSocketMap, map<int, string> &socketClientMap,
                  map<string, set<string>> &groupClientsMap, vector<int> &socketsVec, vector<string> &clientsVec,
                  int curSocket, fd_set &clientsfds);

int main(int argc, char **argv) {
    map<string, int> clientSocketMap;
    map<int, string> socketClientMap;
    map<string, set<string>> groupClientsMap;
    vector<int> socketsVec;
    vector<string> clientsVec;
    socketsVec.push_back(STDIN_FILENO);
    fd_set clientsfds;
    fd_set readfds;
    int serverSockfd, ready;

    if (argc != 2) {
        print_server_usage();
        exit(1);
    }

    unsigned short portnum = getPort(argv);

    serverSockfd = initializeSocket(portnum);

    clientsfds = initializeFdSet(clientsfds, serverSockfd);

    socketsVec.push_back(serverSockfd);

    // We need to listen to stdin and the general socket and 10 clients
    if (listen(serverSockfd, WA_MAX_CLIENTS + 4) < 0) {
        print_error("listen", errno);
        exit(1);
    }
    bool stillRunning = true;
    // This is the main loop requests
    while (stillRunning) {
        readfds = initialiseReadFds(clientsfds, readfds);
        // To be on the safe side we need 500 file descriptors span
        if ((ready = select(500, &readfds, NULL, NULL, NULL)) < 0) {
            print_error("select", errno);
            exit(1);
        }
        // No requests from any clients
        if (ready == 0) {
            continue;
            // When a TCP connection is closed on one side read() on the other side returns 0 byte.
        }
        int curSocket;
        bool changeUsersVec = false;
        for (auto socketNum = socketsVec.begin(); socketNum != socketsVec.end(); ++socketNum) {
            curSocket = *socketNum;
            if FD_ISSET(curSocket, &readfds) {
                // Checking if it's an input from user and if the server exits
                if (sdtinInputCase(curSocket, socketsVec, serverSockfd)) {
                    stillRunning = false;
                    break;
                }
                //bool return because if it returns true, we add int to the vector of the for loop
                if (generalSocketInputCase(socketClientMap, clientsVec, serverSockfd, clientsfds, curSocket,
                                           clientSocketMap, socketsVec,
                                           groupClientsMap)) {
                    break;
                }


                if (curSocket > serverSockfd) {

                    MessageIF msgIF;
                    memset(&msgIF, 0, sizeof(MessageIF));
                    if (recv(curSocket, &msgIF, sizeof(MessageIF), 0) < 0) {
                        print_error("recv", errno);
                        exit(1);
                    }

                    switch (msgIF.typeMsg) {
                        case e_WHO: {
                            MessageIF msgIF;
                            memset(&msgIF, 0, sizeof(MessageIF));
                            msgIF.status = e_SUCCESS;
                            msgIF.typeMsg = e_WHO;
                            string usersForWho;
                            sort(clientsVec.begin(), clientsVec.end());
                            for (auto it = clientsVec.begin(); it != clientsVec.end(); ++it) {
                                usersForWho += *it;
                                if ((it + 1) != clientsVec.end())
                                    usersForWho += ",";
                            }
                            strcpy(msgIF.msg, usersForWho.c_str());
                            if (write(curSocket, &msgIF, sizeof(MessageIF)) < 0) {
                                print_error("write", errno);
                                exit(1);
                            }
                            print_who_server(socketClientMap[curSocket]);
                            break;
                        }
                        case e_CREATE_GROUP: {
                            command_type commandT;
                            string name;
                            string message;
                            vector<string> clients;
                            string castMsg(msgIF.msg);
                            parse_command(castMsg, commandT, name, message, clients);
                            clients.push_back(socketClientMap[curSocket]);
                            msgIF.status = e_SUCCESS;
                            if (groupClientsMap.size() >= WA_MAX_GROUP) {
                                msgIF.status = e_FAILURE;
                            }
                            else if (groupClientsMap.count(name) || clientSocketMap.count(name)) {
                                msgIF.status = e_FAILURE;
                            } else {
                                set<string> groupNamesSet;
                                for (auto it = clients.begin(); it != clients.end(); ++it) {
                                    if (!(find(clientsVec.begin(), clientsVec.end(), *it) != clientsVec.end())) {
                                        msgIF.status = e_FAILURE;
                                        break;
                                    }
                                    groupNamesSet.insert(*it);
                                }
                                if (msgIF.status == e_SUCCESS) {
                                    groupClientsMap[name] = groupNamesSet;
                                }

                            }
                            if (write(curSocket, &msgIF, sizeof(MessageIF)) < 0) {
                                print_error("write", errno);
                                exit(1);
                            }
                            print_create_group(true, msgIF.status == e_SUCCESS, socketClientMap[curSocket], msgIF.name);
                            break;
                        }
                        case e_SEND: {
                            vector<int> clientsToSend;
                            msgIF.status = e_SUCCESS;
                            string nameSent(msgIF.name);
                            string msgSent(msgIF.msg);

                            if (clientSocketMap.count(nameSent)) {
                                clientsToSend.push_back(clientSocketMap[nameSent]);
                            } else if (groupClientsMap.count(nameSent) && groupClientsMap[nameSent].count
                                    (socketClientMap[curSocket])) {
                                //check if he is in this group
                                int num;
                                for (auto it = groupClientsMap[nameSent].begin();
                                     it != groupClientsMap[nameSent].end(); ++it) {
                                    num = clientSocketMap[*it];
                                    if (num != curSocket) {
                                        clientsToSend.push_back(num);
                                    }
                                }
                            } else {
                                msgIF.status = e_FAILURE;
                            }
                            if (msgIF.status == e_SUCCESS) {
                                char nameSender[WA_MAX_NAME + 1];
                                strcpy(nameSender, socketClientMap[curSocket].c_str());
                                strcpy(msgIF.name, nameSender);
                                for (auto it = clientsToSend.begin(); it != clientsToSend.end(); ++it) {
                                    if (write(*it, &msgIF, sizeof(MessageIF)) < 0) {
                                        print_error("write", errno);
                                        exit(1);
                                    }
                                }
                            }
                            msgIF.typeMsg = e_STATUS;
                            if (write(curSocket, &msgIF, sizeof(MessageIF)) < 0){
                                print_error("write", errno);
                                exit(1);
                            };
                            print_send(true, msgIF.status == e_SUCCESS, socketClientMap[curSocket], nameSent, msgSent);
                            break;
                        }
                        case e_EXIT: {
                            userExitCase(clientSocketMap, socketClientMap, groupClientsMap, socketsVec, clientsVec,
                                         curSocket, clientsfds);
                            changeUsersVec = true;
                            break;
                        }
                        default:
                            break;
                    }

                    if (changeUsersVec) {
                        break;
                    }

                }
            }
        }
    }
    return 0;
}


void userExitCase(map<string, int> &clientSocketMap, map<int, string> &socketClientMap,
                  map<string, set<string>> &groupClientsMap, vector<int> &socketsVec, vector<string> &clientsVec,
                  int curSocket, fd_set &clientsfds) {
    string nameOfClinet;
    string deleteGroup;
    nameOfClinet = socketClientMap[curSocket];
    clientsVec.erase(remove(clientsVec.begin(), clientsVec.end(), nameOfClinet), clientsVec.end());
    FD_CLR(curSocket, &clientsfds);
    clientSocketMap.erase(nameOfClinet);
    socketClientMap.erase(curSocket);
    for (auto &groupName : groupClientsMap) {
        if (groupName.second.count(nameOfClinet)) {
            groupName.second.erase(nameOfClinet);
            if (groupName.second.empty()) {
                deleteGroup = groupName.first;
            }
        }
    }
    if (!deleteGroup.empty()) {
        groupClientsMap.erase(deleteGroup);
    }
    socketsVec.erase(remove(socketsVec.begin(), socketsVec.end(), curSocket), socketsVec.end());
    if (close(curSocket) < 0) {
        print_error("close", errno);
        exit(1);
    };
    print_exit(true, nameOfClinet);
}


bool generalSocketInputCase(map<int, string> &socketClientMap, vector<string> &usersVec, int serverSockfd,
                            fd_set &clientsfds, int socketNum, map<string, int> &clientSocketMap,
                            vector<int> &socketsVec, map<string, set<string>> groupClientsMap) {
    if (socketNum == serverSockfd) {
        int cur_socket;

        if ((cur_socket = accept(serverSockfd, NULL, NULL)) < 0) {
            print_error("accept", errno);
            exit(1);
        }
        MessageIF msgIF{};
        if (recv(cur_socket, &msgIF, sizeof(MessageIF), 0) < 0) {
            print_error("recv", errno);
            exit(1);
        }
        string clientName = msgIF.name;
        if ((!usersVec.empty() && std::find(usersVec.begin(), usersVec.end(), clientName) != usersVec.end()) ||
                groupClientsMap.count(clientName)) {
            msgIF.status = e_FAILURE;
            msgIF.typeMsg = e_INITIAL;
        } else {
            FD_SET(cur_socket, &clientsfds);
            clientSocketMap[clientName] = cur_socket;
            socketClientMap[cur_socket] = clientName;
            socketsVec.push_back(cur_socket);
            usersVec.emplace_back(clientName);
            memset(&msgIF, 0, sizeof(MessageIF));
            msgIF.status = e_SUCCESS;
            msgIF.typeMsg = e_INITIAL;
            print_connection_server(clientName);
        }

        if (send(cur_socket, &msgIF, sizeof(MessageIF), 0) < 0) {
            print_error("send", errno);
            exit(1);
        }
        if (msgIF.status == e_FAILURE) {
            if (close(cur_socket) < 0) {
                print_error("close", errno);
                exit(1);
            }
        }
        return true;
    }
    return false;
}

bool sdtinInputCase(int socketNum, vector<int> &socketsVec, int serverSockfd) {
    if (socketNum == STDIN_FILENO) {

        char inputBuf[WA_MAX_MESSAGE ];
        memset(inputBuf, 0, WA_MAX_MESSAGE);
        if (read(STDIN_FILENO, (void *) inputBuf, WA_MAX_MESSAGE) < 0) {
            print_error("read", errno);
            exit(1);
        }
        string str(inputBuf);

        if (str == "EXIT\n") {
            MessageIF msgIF;
            memset(&msgIF, 0 , sizeof(MessageIF));
            msgIF.typeMsg = e_EXIT;
            for(auto it = socketsVec.begin(); it != socketsVec.end(); ++it) {
                if (*it > serverSockfd) {
                    if (write(*it, &msgIF, sizeof(MessageIF)) < 0) {
                        print_error("write", errno);
                        exit(1);
                    }
                    if (close(*it) < 0) {
                        print_error("close", errno);
                        exit(1);
                    }
                }
            }
            print_exit();
            if (close(serverSockfd) < 0) {
                print_error("close", errno);
                exit(1);
            }
            return true;
        } else {
            print_invalid_input();
        }
    }
    return false;
}

fd_set &initialiseReadFds(const fd_set &clientsfds, fd_set &readfds) {
    FD_ZERO(&readfds);
    readfds = clientsfds;
    return readfds;
}

fd_set &initializeFdSet(fd_set &clientsfds, int serverSockfd) {
    FD_ZERO(&clientsfds);
    FD_SET(serverSockfd, &clientsfds);
    FD_SET(STDIN_FILENO, &clientsfds);
    return clientsfds;
}

int initializeSocket(unsigned short portnum) {
    int serverSockfd;
//sockaddrr_in initlization
    serverSockfd = getSocket(portnum);
    return serverSockfd;
}

int getSocket(unsigned short portnum) {
    struct sockaddr_in sa;
    int serverSockfd;
    memset(&sa, 0, sizeof(struct sockaddr_in));
    sa.sin_family = AF_INET;
    /* this is our host address */
    sa.sin_addr.s_addr = INADDR_ANY;
    /* this is our port number */
    sa.sin_port = htons(portnum);

    /* create socket */
    if ((serverSockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        print_error("socket", errno);
        exit(1);
    }

    if (bind(serverSockfd, (struct sockaddr *) &sa, sizeof(struct sockaddr_in)) < 0) {
        print_error("bind", errno);
        close(serverSockfd);
        exit(1);
    }
    return serverSockfd;
}

unsigned short getPort(char *const *argv) {
    istringstream ss(argv[1]);
    istringstream ss2(argv[1]);
    unsigned short portnum;
    string portnumStr;
    if (!(ss >> portnum)) {
        print_server_usage();
        exit(1);
    }
    if (!(ss2 >> portnumStr)) {
        print_server_usage();
        exit(1);
    }
    if (!is_number(portnumStr)) {
        print_invalid_input();
        exit(1);
    }
    return portnum;
}