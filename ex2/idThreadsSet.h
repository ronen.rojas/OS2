#ifndef _IDTHREADSET_H
#define _IDTHREADSET_H

#include <iostream>
#include <set>
#include "uthreads.h"

class IdsSet {

private:

    std::set<int> _threadIdsPool;

public:

    // Constructor for the set
    IdsSet();

    // Get new minimum Id for the set
    int getNewID();

    // retrieve ID back to pool
    void retrieveIDToPool(int id);

};

#endif