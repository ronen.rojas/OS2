#ifndef _THREAD_H
#define _THREAD_H

#include <csetjmp>
#include <iostream>
#include <set>
#include "uthreads.h"


#ifdef __x86_64__
/* code for 64 bit Intel arch */

typedef unsigned long address_t;
#define JB_SP 6
#define JB_PC 7
#else
/* code for 32 bit Intel arch */
typedef unsigned int address_t;
#define JB_SP 4
#define JB_PC 5
/* A translation is required when using an address of a variable.
   Use this as a black box in your code. */
#endif

address_t translate_address(address_t addr);

enum State {
    e_READY, e_RUNNING, e_BLOCKED
};

class Thread {

private:
	
	// The state of the thread
    State _state;

	// A set of ID's of all the threads this thread has synced
    std::set<int> _syncSet;

	// the ID of the thread
    int _id;

    // Independent flag to notify if the thread has been synced
    bool _flagSynced;

	// The Buffer of the state for jump purposes 
    sigjmp_buf _env;

	// Allocated stack pointer in the heap.
    char *_stack;

	// quantum of the thread.
    int _quantum;

    // A flag for empty Set.
    bool _emptyMask;

public:

    // Constructor for the all the threads spawned
    Thread(void (*newThreadFunc)(void));

    Thread(int newID, void (*newThreadFunc)());

    // Destructor
    ~Thread();

    // Setter for the state
    void setState(State newState);

    // Getter for the state
    State getState();

    // Setter for synced flag
    void setFlagSynced(bool isSyncedFlag);

    // Getter for synced flag
    bool isSynced();

    // Getter for id
    int getID();

    // Add a thread to the synced list
    void addToSyncList(int id);

    // Getter of the sync set 
    std::set<int> *getSyncSet();

    // Getter of the context of the thread ( Stack and pc)
    sigjmp_buf *getEnv();

    // Increase Quantum thread
    void increaseQuantum();

    // Get Quantum thread
    int getQuantum();

    // Get Empty mask flag
    bool getEmptyMask();
};

#endif /* _THREAD_H */
