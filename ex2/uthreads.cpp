
#include <cstdio>
#include <csetjmp>
#include <csignal>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <map>
#include <list>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <algorithm>
#include "idThreadsSet.h"
#include "Thread.h"


struct sigaction saTimer;
struct itimerval timer;

void maskTimerSignal();

void unMaskTimerSignal();

void exit_handler();

IdsSet *idsSetpool;
std::map<int, Thread *> threadDict;
std::list<int> readyList;
Thread *threadRunning;
Thread *threadMain;
int totalQuantum;
sigset_t signalSet;

// Auxiliary function

void stam() {
}

void maskTimerSignal() {

    if (sigprocmask(SIG_BLOCK, &signalSet, NULL) < 0) {
        std::cerr << "system error: sigprocmask function signal block error - " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    }
};


void unMaskTimerSignal() {
    // A system call to unblock the signals in the set : signalSet

    if (sigprocmask(SIG_UNBLOCK, &signalSet, NULL) < 0) {
        std::cerr << "system error: sigprocmask function signal unblock error  -  " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    }
};

void schedulerThread(State state) {
    maskTimerSignal();

    // For any scheduling event the global quantum needs to increase
    totalQuantum++;

    if (readyList.empty()) {
        // READY queue is empty if the main thread is running
        threadRunning->increaseQuantum();
    } else {
        // Getting the next thread ID for next running thread.
        int nextID = readyList.front();
        readyList.pop_front();

        int retVal = 0;
        switch (state) {
            case e_READY  : {
                // This scheduling event came from the normal ticking of the quantum timer (timer_handler)
                // The running thread should pushed to the end of the READY queue
                // its state should be saved in case of jumping to it when changing from READY to running
                int runningID = threadRunning->getID();
                readyList.push_back(runningID);
                threadRunning->setState(e_READY);

                // Making sure that the Main thread will not be saved
                retVal = sigsetjmp(*(threadRunning->getEnv()), 1);
                break;
            }

            case e_BLOCKED :
                // This scheduling event came from a self blocking thread  -- (uthread_block)
                // Or when a uthread_sync is called consequently leading to blocking the running thread -- (uthread_sync)
                // The running thread should change itself to block.
                // its state should be saved in case of jumping to it when changing from READY to running ( if ever released/synced back)

                threadRunning->setState(e_BLOCKED);
                retVal = sigsetjmp(*(threadRunning->getEnv()), 1);
                break;

            case e_RUNNING :
                // This scheduling event came from terminating the running thread
                // Its state should not be saved obviously because of the termination.
                // We need to emulate as if the sigsetjmp came back from a save
                retVal = 0;
            default:
                break;
        }

        if (retVal == 0) {
            // coming back from a save of sigsetjmp meaning we need run the next thread in READY
            // new running thread pointer

            threadRunning = threadDict[nextID];
            threadRunning->setState(e_RUNNING);
            threadRunning->increaseQuantum();
            siglongjmp(*(threadRunning->getEnv()), 1);
        }
        // Making sure will not be jumping to the Main Thread

        // coming straight here after a jump
    }

    // Setting a new timer for the next quantum
    if (setitimer(ITIMER_VIRTUAL, &timer, NULL)) {
        std::cerr << "system error: setitimer function set timer error -  " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    }
    unMaskTimerSignal();
}

void timer_handler(int sigNum) {
    // Regular scheduling task - preempt the thread.
    schedulerThread(e_READY);
}

void exit_handler() {
    // A function that is called whenever we need to exit the process and
    // we need to delete of the objects of the threads.
    std::map<int, Thread *>::iterator itThreads;

    // Clearing of the thread objects
    for (itThreads = threadDict.begin(); itThreads != threadDict.end(); ++itThreads) {
        Thread *localThread = itThreads->second;
        delete localThread;
    }
    delete idsSetpool;


}

// Library Functions

int uthread_init(int quantum_usecs) {

    // Error check quantum_usecs is non-positive
    if (quantum_usecs <= 0) {
        std::cerr << "thread library error: uthread_init - non-positive quantum_usecs" << std::endl;
        return -1;
    }

    // Instantiating the main thread
    threadMain = new Thread(0, stam);

    if (!threadMain->getEmptyMask()) {
        std::cerr << "system error: sigemptyset function  - empty set error -  " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    };
    threadDict[0] = threadMain;


    // Initialise pool for IDs : {1}
    idsSetpool = new IdsSet();

    // Instantiating the running thread from main
    threadRunning = threadMain;
    threadRunning->setState(e_RUNNING);
    threadRunning->increaseQuantum();
    totalQuantum++;

    // Initialise an empty signal set : {}
    if (sigemptyset(&signalSet) < 0) {
        std::cerr << "system error: sigemptyset function  - empty set error -  " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    }

    // Add SIGVTALRM signal to the signal set : {SIGVTALRM}
    if (sigaddset(&signalSet, SIGVTALRM) < 0) {
        std::cerr << "system error: sigaddset   - function adding signal set error  -  " << strerror(errno)
                  << std::endl;
        exit_handler();
        exit(1);
    }



    // Install timer_handler as the signal handler for SIGVTALRM.
    saTimer.sa_handler = &timer_handler;
    if (sigemptyset(&saTimer.sa_mask) < 0) {
        std::cerr << "system error: sigemptyset function  - empty set error  -  " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    }
    if (sigaction(SIGVTALRM, &saTimer, NULL) < 0) {

        std::cerr << "system error: could not handle sigaction for the signal  SIGVTALRM error - " << strerror(errno)
                  << std::endl;
        exit_handler();
        exit(1);
    }

    // Initialise Timer for SIGVTALRM
    // Flooring the seconds
    int seconds = floor(quantum_usecs / 1000000);
    // Reminder in  micro-seconds
    int microseconds = quantum_usecs - (seconds * 1000000);
    timer.it_value.tv_sec = seconds;            // first time interval, seconds part
    timer.it_value.tv_usec = microseconds;        // first time interval, microseconds part

    // configure the timer to expire every 0 sec after that.
    timer.it_interval.tv_sec = seconds;    // following time intervals, seconds part
    timer.it_interval.tv_usec = microseconds;    // following time intervals, microseconds part

    // Start a virtual timer. It counts down whenever this process is executing.
    if (setitimer(ITIMER_VIRTUAL, &timer, NULL)) {

        std::cerr << "system error: setitimer function set timer error -  " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    }
    return 0;
};


int uthread_spawn(void (*f)(void)) {
    maskTimerSignal();

    // Error check if *f is a NULL pointer
    if (f == NULL) {
        std::cerr << "thread library error: uthread_spawn - f Null pointer" << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    int tid = idsSetpool->getNewID();
    // Error check over MAX_THREAD_NUM threads
    if (tid == -1) {
        std::cerr << "thread library error: uthread_spawn - Too many Threads" << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    // Instantiating a local thread object , adding it the dictionary
    Thread *threadLocal = new Thread(tid, f);

    if (!threadLocal->getEmptyMask()) {
        std::cerr << "system error: sigemptyset function  - empty set error -  " << strerror(errno) << std::endl;
        exit_handler();
        exit(1);
    };

    threadDict[tid] = threadLocal;
    // pushing it to the end of the READY queue
    readyList.push_back(tid);

    unMaskTimerSignal();
    return tid;
};


int uthread_terminate(int tid) {

    maskTimerSignal();

    if (tid == 0) {
        // The main thread has ended we need to prevent memory leaks
        exit_handler();
        // This function does not return in this case
        exit(0);
    }


    // Check if thread exist
    if (threadDict.count(tid) == 0) {
        std::cerr << "thread library error: uthread_terminate - no such thread id " << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    // Iterate throughout all of the synced threads and change their _flagSynced to false
    std::set<int> syncedSet = *threadDict[tid]->getSyncSet();
    std::set<int>::iterator itSyncedSet;

    for (itSyncedSet = syncedSet.begin(); itSyncedSet != syncedSet.end(); ++itSyncedSet) {
        threadDict[*itSyncedSet]->setFlagSynced(false);
        threadDict[*itSyncedSet]->setState(e_READY);
        readyList.push_back(*itSyncedSet);
    }


    // Instantiating the to-be terminated thread.
    Thread *localThread = threadDict[tid];
    State threadState = threadDict[tid]->getState();
    threadDict.erase(tid);
    idsSetpool->retrieveIDToPool(tid);

    switch (threadState) {
        case e_RUNNING :
            delete localThread;

            // Explanation We want to go to a special case the scheduler Thread where we don't need
            // to save any state just jump to the next thread in ready
            // This function does not return in this case - special case of scheduling.
            schedulerThread(e_RUNNING);
            break;
        case e_READY:
            // Remove from the READY queue

            readyList.remove(tid);
            break;
        default :
            // BLOCKED : do nothing
            break;
    }
    // Remove the thread.
    delete localThread;

    unMaskTimerSignal();
    return 0;
};

int uthread_block(int tid) {

    maskTimerSignal();
    // Check if thread exist
    if (threadDict.count(tid) == 0) {
        std::cerr << "thread library error: uthread_block - no such thread id " << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    // Check if thread is the main thread
    if (tid == 0) {
        unMaskTimerSignal();
        std::cerr << "thread library error: uthread_block - trying to block the main thread " << std::endl;
        return -1;
    }

    switch (threadDict[tid]->getState()) {
        case e_RUNNING :
            // The Thread is blocking itself
            // Explanation We want to go to a special case the scheduler Thread where we don't need
            // to save any state just jump to the next thread in ready
            schedulerThread(e_BLOCKED);
            break;
        case e_READY:
            // Remove for READY queue & change status to blocked
            readyList.remove(tid);
            threadDict[tid]->setState(e_BLOCKED);
            break;
        default :
            // BLOCKED : Nothing to do already blocked
            break;
    }
    unMaskTimerSignal();
    return 0;
};


int uthread_resume(int tid) {

    maskTimerSignal();

    // Check if such thread exist
    if (threadDict.count(tid) == 0) {
        std::cerr << "thread library error: uthread_resume - no such thread id " << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    // If the thread needs to be synced back do not resume , if it's already in READY or RUNNING do nothing
    if (threadDict[tid]->isSynced() || threadDict[tid]->getState() == e_READY ||
        threadDict[tid]->getState() == e_RUNNING) {
        unMaskTimerSignal();
        return 0;
    };

    // The thread BLOCKED state and not synced, change status to ready and push to the end of the READY queue
    threadDict[tid]->setState(e_READY);
    readyList.push_back(tid);

    unMaskTimerSignal();
    return 0;
};


int uthread_sync(int tid) {

    // Check if thread exists.
    maskTimerSignal();
    if (threadDict.count(tid) == 0) {
        std::cerr << "thread library error: uthread_sync - no such thread id" << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    // Check if main thread is calling to sync
    if (threadRunning->getID() == 0) {
        std::cerr << "thread library error: uthread_sync - try to sync the main thread" << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    // Check if the Running thread trying to synced itself.
    if (threadDict[tid]->getState() == e_RUNNING) {
        std::cerr << "thread library error: uthread_sync - try to sync itself" << std::endl;
        unMaskTimerSignal();
        return -1;
    }

    // Add a the synced flag to the running thread
    threadRunning->setFlagSynced(true);
    // Add the running thread id to the synced set of thread tid.
    threadDict[tid]->addToSyncList(threadRunning->getID());
    // Scheduling task similar to block.
    schedulerThread(e_BLOCKED);
    unMaskTimerSignal();
    return 0;
};


int uthread_get_tid() {
    maskTimerSignal();

    // Getting the id of the running thread
    int tid = threadRunning->getID();
    unMaskTimerSignal();
    return tid;
};


int uthread_get_total_quantums() {
    // Getting the total quantum count.
    return totalQuantum;
};

int uthread_get_quantums(int tid) {
    maskTimerSignal();

    // Check if such thread exists.
    if (threadDict.count(tid) == 0) {
        std::cerr << "thread library error: uthread_get_quantums- no such thread id " << std::endl;
        unMaskTimerSignal();
        return -1;
    }
    // If it exists getting the counter count of the tid thread.
    int quantumValue = threadDict[tid]->getQuantum();
    unMaskTimerSignal();

    return quantumValue;
};
