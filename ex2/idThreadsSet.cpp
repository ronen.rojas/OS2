#include <iostream>
#include <set>
#include "idThreadsSet.h"

IdsSet::IdsSet() {

    // Initialise the pool of IDS to be the set : {1}
    _threadIdsPool.insert(1);
}

int IdsSet::getNewID() {

    // Get the minimum ID got the pool
    int id = *_threadIdsPool.begin();

    _threadIdsPool.erase(id);

    // If the pool set is empty add another ID to the set {min_id + 1}
    if (_threadIdsPool.empty()) { _threadIdsPool.insert(id + 1); }

    // IF the minimum ID is 100 there are more then 100 THREAD in different state return Error
    if (id == MAX_THREAD_NUM) { return -1; }

    return id;
}

void IdsSet::retrieveIDToPool(int id) { _threadIdsPool.insert(id); }

