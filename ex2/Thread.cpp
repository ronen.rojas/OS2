#include <csetjmp>
#include <iostream>
#include <set>
#include "Thread.h"
#include <cstdio>
#include <csetjmp>
#include <csignal>
#include <unistd.h>
#include <sys/time.h>

#ifdef __x86_64__
/* code for 64 bit Intel arch */
/* A translation is required when using an address of a variable.
   Use this as a black box in your code. */
address_t translate_address(address_t addr) {
    address_t ret;
    asm volatile("xor    %%fs:0x30,%0\n"
            "rol    $0x11,%0\n"
    : "=g" (ret)
    : "0" (addr));
    return ret;
}

#else
/* code for 32 bit Intel arch */
/* A translation is required when using an address of a variable.
   Use this as a black box in your code. */
address_t translate_address(address_t addr)
{
    address_t ret;
    asm volatile("xor    %%gs:0x18,%0\n"
            "rol    $0x9,%0\n"
    : "=g" (ret)
    : "0" (addr));
    return ret;
}
#endif

Thread::Thread(int newID, void (*newThreadFunc)(void)) : _state(e_READY),
_id(newID), _flagSynced(false), _emptyMask(true) {

    address_t sp, pc;

    // Allocating stack of the thread in the heap
    _stack = new char[STACK_SIZE];

    // Using the black box
    sp = (address_t) _stack + STACK_SIZE - sizeof(address_t);
    pc = (address_t) newThreadFunc;
    sigsetjmp(_env, 1);
    (_env->__jmpbuf)[JB_SP] = translate_address(sp);
    (_env->__jmpbuf)[JB_PC] = translate_address(pc);

    if (sigemptyset(&_env->__saved_mask) < 0){
        _emptyMask = false;
	};

}

Thread::~Thread() { delete _stack; }

void Thread::setState(State newState) { _state = newState; }

State Thread::getState() { return _state; }

void Thread::setFlagSynced(bool isSyncedFlag) { _flagSynced = isSyncedFlag; }

bool Thread::isSynced() { return _flagSynced; }

void Thread::addToSyncList(int id) { _syncSet.insert(id); }

std::set<int> *Thread::getSyncSet() { return &_syncSet; }

int Thread::getID() { return _id; }

sigjmp_buf *Thread::getEnv() { return &_env; }

void Thread::increaseQuantum() { _quantum++; };

int Thread::getQuantum() { return _quantum; };

bool Thread::getEmptyMask() { return _emptyMask; };
