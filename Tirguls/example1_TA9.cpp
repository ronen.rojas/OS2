//
// Created by ronenr on 7/5/18.
//

#include <cstdio>
#include <cstdlib>
#include <netdb.h>
#include <arpa/inet.h>


int main(int argc, char *argv[]){
    struct hostent *h;
    if (argc !=2) {
        fprintf(stderr, "usage: getip addres\n");
        exit(1);
    }

    if ((h=gethostbyname(argv[1]))== NULL){
        fprintf(stderr, "gethost\n");
        exit(1);
    }
    printf("Host name : %s\n",h->h_name);
    printf("IP Address: %s\n",inet_ntoa(*((struct in_addr *)h->h_addr)));
}