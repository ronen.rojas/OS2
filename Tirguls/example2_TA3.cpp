#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void catch_int(int sig_num) {
	printf("Don't do that\n");
	fflush(stdout);

} 

int main (int argc, char* argv[]) {
	struct sigaction sa;
	sa.sa_handler = &catch_int;  
	sigaction(SIGINT, &sa, NULL);
	for (;;)
		pause();
}
