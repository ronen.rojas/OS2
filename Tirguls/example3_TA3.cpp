#include <setjmp.h>
#include <stdio.h>


int main (int argc, char* argv[]) {
	sigjmp_buf jbuf;
	int i= 10;  
	int ret_val = sigsetjmp(jbuf, 1);
	if ( ret_val == 1) { 
		return 0;
	}
	i--;
	printf("hello\n");
	siglongjmp(jbuf, i);
	return 0;
}
