#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void catch_int(int sig_num) {
	signal(SIGINT, catch_int);
	printf("Don't do that\n");
	fflush(stdout);

} 

int main (int argc, char* argv[]) { 
	signal(SIGINT, catch_int);
	for (;;)
		pause();
}
